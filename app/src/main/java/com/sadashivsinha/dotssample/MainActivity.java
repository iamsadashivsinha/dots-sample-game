package com.sadashivsinha.dotssample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int currentScore = 0;

    public enum COLORS {RED, GREEN, BLUE, BROWN, PINK, YELLOW, PURPLE, ORANGE}

    int[][] dots = new int[4][4];

    List<CircleImageView> connectionsDots = new ArrayList<>();

    Button btnClear;
    FrameLayout rootLayout;
    TextView tvScore;

    float startX;
    float startY;
    float stopX;
    float stopY;

    Paint paint;

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViews();
    }

    public void setupViews() {
        tvScore = findViewById(R.id.tv_score);
        btnClear = findViewById(R.id.btn_clear);
        rootLayout = findViewById(R.id.root_layout);
        btnClear.setOnClickListener(this);

        dots[0][0] = R.id.dot_one_one;
        dots[0][1] = R.id.dot_one_two;
        dots[0][2] = R.id.dot_one_three;
        dots[0][3] = R.id.dot_one_four;

        dots[1][0] = R.id.dot_two_one;
        dots[1][1] = R.id.dot_two_two;
        dots[1][2] = R.id.dot_two_three;
        dots[1][3] = R.id.dot_two_four;

        dots[2][0] = R.id.dot_three_one;
        dots[2][1] = R.id.dot_three_two;
        dots[2][2] = R.id.dot_three_three;
        dots[2][3] = R.id.dot_three_four;

        dots[3][0] = R.id.dot_four_one;
        dots[3][1] = R.id.dot_four_two;
        dots[3][2] = R.id.dot_four_three;
        dots[3][3] = R.id.dot_four_four;

        createNewGame();

    }

    public void createNewGame() {
        updateScores(0);

        addRandomDots((CircleImageView) findViewById(dots[0][0]), 0, 0);
        addRandomDots((CircleImageView) findViewById(dots[0][1]), 0, 1);
        addRandomDots((CircleImageView) findViewById(dots[0][2]), 0, 2);
        addRandomDots((CircleImageView) findViewById(dots[0][3]), 0, 3);

        addRandomDots((CircleImageView) findViewById(dots[1][0]), 1, 0);
        addRandomDots((CircleImageView) findViewById(dots[1][1]), 1, 1);
        addRandomDots((CircleImageView) findViewById(dots[1][2]), 1, 2);
        addRandomDots((CircleImageView) findViewById(dots[1][3]), 1, 3);

        addRandomDots((CircleImageView) findViewById(dots[2][0]), 2, 0);
        addRandomDots((CircleImageView) findViewById(dots[2][1]), 2, 1);
        addRandomDots((CircleImageView) findViewById(dots[2][2]), 2, 2);
        addRandomDots((CircleImageView) findViewById(dots[2][3]), 2, 3);

        addRandomDots((CircleImageView) findViewById(dots[3][0]), 3, 0);
        addRandomDots((CircleImageView) findViewById(dots[3][1]), 3, 1);
        addRandomDots((CircleImageView) findViewById(dots[3][2]), 3, 2);
        addRandomDots((CircleImageView) findViewById(dots[3][3]), 3, 3);
    }

    public void setDotColors(CircleImageView dot, int color, COLORS colorTag, int x, int y) {
        dot.setImageResource(color);
        dot.setBorderColor(color);
        dot.setTag(R.string.KEY_COLOR, colorTag);
        dot.setTag(R.string.KEY_X, x);
        dot.setTag(R.string.KEY_Y, y);

        dot.setOnClickListener(this);
    }

    public boolean validMove(int oneX, int oneY, int twoX, int twoY) {
        if (oneX == twoX) {
            if ((oneY > twoY ? oneY - twoY : twoY - oneY) == 1) {
                // row sibling
                return true;
            }
            else return false;
        }
        else if (oneY == twoY) {
            if ((oneX > twoX ? oneX - twoX : twoX - oneX) == 1) {
                // column sibling
                return true;
            }
            else return false;
        }
        else return false;
    }

    public void clearGame(boolean allClear, boolean newGame) {
        connectionsDots.clear();
        connectionsDots = new ArrayList<>();

        for (int i = 0; i < dots.length; i++)
            for (int j = 0; j < dots.length; j++)
                ((CircleImageView) findViewById(dots[i][j])).setBorderWidth(0);

        if (allClear) rootLayout.removeAllViews();
        if (newGame) createNewGame();

        Toast.makeText(this, "Cleared", Toast.LENGTH_SHORT).show();
    }

    public class CanvasView extends View {

        public CanvasView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine(startX, startY, stopX, stopY, paint);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onClick(View view) {
        if (handler != null && handler.hasCallbacks(runnable)) handler.removeCallbacks(runnable);

        if (view.equals(btnClear)) {
            clearGame(true, true);
        }
        else if (view instanceof CircleImageView) {

            if (connectionsDots.size() > 0) {

                if (validMove((Integer) connectionsDots.get(connectionsDots.size() - 1).getTag(R.string.KEY_X), (Integer) connectionsDots.get(connectionsDots.size() - 1).getTag(R.string.KEY_Y), (Integer) view.getTag(R.string.KEY_X), (Integer) view.getTag(R.string.KEY_Y))) {

                    if (connectionsDots.get(connectionsDots.size() - 1).getTag(R.string.KEY_COLOR) == view.getTag(R.string.KEY_COLOR)) {
                        Toast.makeText(this, "Matched : " + view.getTag(R.string.KEY_COLOR), Toast.LENGTH_SHORT).show();

                        connectionsDots.get(connectionsDots.size() - 1).setBorderWidth(10);
                        ((CircleImageView) view).setBorderWidth(10);

                        int[] location = new int[2];
                        connectionsDots.get(connectionsDots.size() - 1).getLocationOnScreen(location);

                        startX = location[0] + 150;
                        startY = location[1] - 150;

                        location = new int[2];
                        view.getLocationOnScreen(location);

                        stopX = location[0] + 150;
                        stopY = location[1] - 150;

                        Log.d("Points_Val : ", startX + "," + startY + "," + stopX + "," + stopY);

                        paint = new Paint();
                        paint.setColor(getResources().getColor(connectionsDots.get(connectionsDots.size() - 1).getBorderColor()));
                        paint.setStrokeWidth(30);
                        paint.setStyle(Paint.Style.STROKE);

                        rootLayout.addView(new CanvasView(this));
                        connectionsDots.add((CircleImageView) view);

                        runnable = new Runnable() {
                            @Override
                            public void run() {

                                int dotsMatchedSize = connectionsDots.size();
                                updateScores(dotsMatchedSize);

                                for (int i = 0; i < dotsMatchedSize; i++) {
                                    addRandomDots(connectionsDots.get(i), (int) connectionsDots.get(i).getTag(R.string.KEY_X), (int) connectionsDots.get(i).getTag(R.string.KEY_Y));
                                }

                                clearGame(true, false);
                            }
                        };

                        handler = new Handler();
                        handler.postDelayed(runnable, 1500);

                    }
                    else {
                        Log.d("Clear_Reason : ", "Color not match");
                        connectionsDots.add((CircleImageView) view);
                    }
                }
                else {
                    Log.d("Clear_Reason : ", "Not a valid move");
                    connectionsDots.add((CircleImageView) view);
                }
            }
            else {
                Log.d("Clear_Reason : ", "First element");
                connectionsDots.add((CircleImageView) view);
            }
        }
    }

    public void updateScores(int score) {
        if (score == 0) currentScore = 0;
        else currentScore = currentScore + score;

        tvScore.setText(String.valueOf(currentScore));
    }

    public void addRandomDots(CircleImageView civ, int keyX, int keyY) {
        Random r = new Random();
        int result = r.nextInt(7);

        Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        civ.startAnimation(slideUp);

        switch (result) {
            case 0:
                setDotColors(civ, R.color.red, COLORS.RED, keyX, keyY);
                break;

            case 1:
                setDotColors(civ, R.color.green, COLORS.GREEN, keyX, keyY);
                break;

            case 2:
                setDotColors(civ, R.color.blue, COLORS.BLUE, keyX, keyY);
                break;

            case 3:
                setDotColors(civ, R.color.brown, COLORS.BROWN, keyX, keyY);
                break;

            case 4:
                setDotColors(civ, R.color.pink, COLORS.PINK, keyX, keyY);
                break;

            case 5:
                setDotColors(civ, R.color.yellow, COLORS.YELLOW, keyX, keyY);
                break;

            case 6:
                setDotColors(civ, R.color.purple, COLORS.PURPLE, keyX, keyY);
                break;

            case 7:
                setDotColors(civ, R.color.orange, COLORS.ORANGE, keyX, keyY);
                break;
        }
    }
}
